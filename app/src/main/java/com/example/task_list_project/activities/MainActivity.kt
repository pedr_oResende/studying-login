package com.example.task_list_project.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.navigation.findNavController
import com.example.task_list_project.R
import com.example.task_list_project.database.UsersDatabase
import com.example.task_list_project.databinding.ActivityMainBinding
import com.example.task_list_project.repository.UserRepositoryImpl


class MainActivity : AppCompatActivity() {


    companion object {
        private lateinit var binding: ActivityMainBinding

        private val database: UsersDatabase by lazy {
            UsersDatabase.buildDatabase(binding.root.context)
        }

        val repository by lazy {
            UserRepositoryImpl(database.userDao(), database.taskDao(), database.groupDao())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }

    override fun onBackPressed() {
        val mainFragments = listOf("fragment_login", "fragment_home")

        val navController = findNavController(R.id.nav_host_fragment).currentDestination!!
        if (mainFragments.contains(navController.label.toString())) {
            this.finish()
        } else {
            super.onBackPressed()
        }
    }
}