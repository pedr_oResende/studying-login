package com.example.task_list_project.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "groups")
class Group(
    @PrimaryKey
    val id: String = UUID.randomUUID().toString(),
    var leaderId: String?,
    var membersId: String?,
    var name: String,
    var description: String
) {
    fun isNameValid() : Boolean {
        return name.isNotEmpty()
    }
}