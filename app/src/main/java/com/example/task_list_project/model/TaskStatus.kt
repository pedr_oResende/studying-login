package com.example.task_list_project.model

enum class TaskStatus {
    TODO, IN_PROGRESS, DONE;
}