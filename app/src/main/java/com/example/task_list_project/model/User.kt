package com.example.task_list_project.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "users")
class User(
    @PrimaryKey
    var id: String = UUID.randomUUID().toString(),
    val username: String,
    val email: String,
    val password: String,
    var isOnline: Boolean = false,
    var groupsId: String = ""
) {
    fun isPasswordValid(): Boolean {
        return password.length == 6
    }

    fun isUsernameValid(): Boolean {
        return username.isNotEmpty()
    }

    fun isEmailValid(): Boolean {
        return email.isNotEmpty() and email.contains("@")
    }

    fun matchPassword(passwordTyped: ArrayList<Pair>): Boolean {
        for (i in 0..4) {
            val digit = password[i].code - '0'.code
            if (!passwordTyped[i].contains(digit))
                return false
        }
        return true
    }

    fun matchEmail(emailTyped: String): Boolean {
        if (!isEmailValid())
            return false

        return email == emailTyped
    }

}