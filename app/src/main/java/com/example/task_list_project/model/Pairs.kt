package com.example.task_list_project.model

import kotlin.random.Random

class Pairs {

    private val passwordDigits = arrayListOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)

    var pairs = arrayListOf(Pair())

    private val remainingDigits = arrayListOf<Int>()

    init {
        remainingDigits.addAll(passwordDigits)
        pairs.removeAt(0)
        pairs = setupPairs()
    }

    private fun setupPairs(): ArrayList<Pair> {
        for (i in 0..4) {
            val first = generatePairValue()
            val second = generatePairValue()
            val pair = Pair(first, second)
            pair.sort()
            pairs.add(pair)
        }
        pairs.sortBy { it.getFirst() }
        return pairs
    }

    private fun setupRandom(max: Int): Int {
        var n = -1
        while (!remainingDigits.contains(n)) {
            n = Random.nextInt(max)
        }
        return n
    }

    private fun generatePairValue(): Int {
        val value = setupRandom(passwordDigits.size)
        remainingDigits.remove(value)
        return value
    }

}