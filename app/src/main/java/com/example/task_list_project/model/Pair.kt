package com.example.task_list_project.model

data class Pair(private var first: Int = -1, private var second: Int = -1) {

    fun getFirst() = first

    fun getSecond() = second

    fun sort() {
        if (second < first) {
            val temp = second
            second = first
            first = temp
        }
    }

    fun contains(n: Int): Boolean {
        return (n == first) || (n == second)
    }
}