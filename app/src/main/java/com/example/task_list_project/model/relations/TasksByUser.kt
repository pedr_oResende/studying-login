package com.example.task_list_project.model.relations

import androidx.room.Embedded
import androidx.room.Relation
import com.example.task_list_project.model.Task
import com.example.task_list_project.model.User

data class TasksByUser(
    @Embedded
    val user: User,
    @Relation(parentColumn = "id", entityColumn = "userTaskId")
    val tasks: List<Task>?
)