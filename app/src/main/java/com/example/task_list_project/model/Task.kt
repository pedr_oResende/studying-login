package com.example.task_list_project.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.task_list_project.R
import java.util.*

@Entity
class Task(
    @PrimaryKey
    val id: String = UUID.randomUUID().toString(),
    var title: String,
    var content: String,
    var deadline: String,
    var priority: Int,
    @ColumnInfo(name = "userTaskId")
    var userId: String,
    var groupId: String,
    var status: TaskStatus = TaskStatus.TODO,
    var comments: String = ""
) {
    companion object {

        const val CONTENT_SIZE_LIMIT_IN_LIST = 25
        const val CONTENT_SIZE_LIMIT = 40

        fun getPriorityIcon(value: Int): Int {
            return when (value) {
                3 -> R.drawable.ic_double_arrow_up
                else -> R.drawable.ic_arrow_up
            }
        }

        fun getPriorityIconColor(value: Int): Int {
            return when (value) {
                3 -> R.color.high_priority
                2 -> R.color.medium_priority
                else -> R.color.low_priority
            }
        }
    }

    fun isTitleValid(): Boolean {
        return title.isNotEmpty()
    }

    fun isContentValid(): Boolean {
        return content.isNotEmpty() && content.length <= CONTENT_SIZE_LIMIT
    }

    fun isDeadlineValid() : Boolean {
        return deadline.isNotEmpty()
    }
}