package com.example.task_list_project.fragments

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.get
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R
import com.example.task_list_project.activities.MainActivity
import com.example.task_list_project.databinding.FragmentTaskDetailBinding
import com.example.task_list_project.model.Task
import com.example.task_list_project.model.TaskStatus
import com.example.task_list_project.recyclerView.TaskCommentsAdapter
import com.example.task_list_project.viewmodels.TaskDetailViewModel
import kotlinx.coroutines.launch

class TaskDetailFragment : Fragment(), TaskCommentsAdapter.TaskDetailFragmentListener {

    private var _binding: FragmentTaskDetailBinding? = null
    private val binding get() = _binding!!

    private val viewModel: TaskDetailViewModel by viewModels()
    private val repository by lazy { MainActivity.repository }

    private lateinit var task: Task
    private lateinit var taskCommentsRecyclerView: RecyclerView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTaskDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            setupRecyclerView()
            setTask()
            viewModel.status.observe(viewLifecycleOwner, {
                setupTaskDetailStatusText()
            })
            viewModel.comments.observe(viewLifecycleOwner, {
                updateTask()
                updateRecyclerView(viewModel.getCommentsArray())
            })
            setupToolBar()
            setupTaskDetail()
            changeStatusButtonCLick()
            addCommentClickListener()
        }
    }

    private fun setupToolBar() {
        val requireActivity = (requireActivity() as AppCompatActivity)
        requireActivity.setSupportActionBar(binding.toolbar)
        requireActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        requireActivity.supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun setupRecyclerView() {
        taskCommentsRecyclerView = binding.taskDetailComments
        taskCommentsRecyclerView.layoutManager = LinearLayoutManager(binding.root.context)
        updateRecyclerView(viewModel.getCommentsArray())

    }

    private fun updateRecyclerView(comments: ArrayList<String>) {
        if (comments.size > 0) {
            lifecycleScope.launch {
                taskCommentsRecyclerView.adapter = TaskCommentsAdapter(
                    comments,
                    this@TaskDetailFragment,
                    repository.getOnlineUser()!!.id != task.userId
                )
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_task_detail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        setEditMenuVisibility(menu)
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                binding.root.findNavController().navigateUp()
                true
            }
            R.id.menu_edit -> {
                goToUpdateTask(task)
                true
            }
            else -> onOptionsItemSelected(item)
        }
    }

    private fun setEditMenuVisibility(menu: Menu) {
        arguments?.let {
            val isEditable = TaskDetailFragmentArgs.fromBundle(it).isEditable
            menu[0].isVisible = isEditable
        }
    }

    private suspend fun setTask() {
        arguments?.let {
            val taskId = TaskDetailFragmentArgs.fromBundle(it).taskId
            task = repository.getTaskById(taskId)!!
            viewModel.changeStatus(task.status)
            viewModel.setComments(task.comments)
        }
    }

    private fun updateTask() {
        task.comments = viewModel.getComments()
    }

    private fun setupTaskDetailStatusButton() {
        binding.changeStatusButton.isVisible = false
        when (viewModel.getStatus()) {
            TaskStatus.TODO -> {
                binding.backToDo.isVisible = false
                binding.forwardDone.isVisible = false
                binding.forwardInProgress.isVisible = true
            }
            TaskStatus.IN_PROGRESS -> {
                binding.backToDo.isVisible = true
                binding.forwardDone.isVisible = true
                binding.forwardInProgress.isVisible = false
            }
            TaskStatus.DONE -> {
                binding.backToDo.isVisible = true
                binding.forwardDone.isVisible = false
                binding.forwardInProgress.isVisible = false
            }
        }
    }

    private fun changeTaskStatus(status: TaskStatus) {
        viewModel.changeStatus(status)
        task.status = viewModel.getStatus()
        lifecycleScope.launch { repository.updateTask(task) }
        hideChangeStatusButton()
    }

    private fun hideChangeStatusButton() {
        binding.backToDo.isVisible = false
        binding.forwardDone.isVisible = false
        binding.forwardInProgress.isVisible = false
        binding.changeStatusButton.isVisible = true
    }

    private fun changeStatusButtonCLick() {
        binding.changeStatusButton.setOnClickListener {
            setupTaskDetailStatusButton()
        }
        binding.backToDo.setOnClickListener {
            changeTaskStatus(TaskStatus.TODO)
        }
        binding.forwardInProgress.setOnClickListener {
            changeTaskStatus(TaskStatus.IN_PROGRESS)
        }
        binding.forwardDone.setOnClickListener {
            changeTaskStatus(TaskStatus.DONE)
        }
    }

    private fun setupTaskDetail() {
        binding.taskDetailTitle.text = task.title
        binding.taskDetailContent.text = task.content
        binding.taskDetailPriority.setImageResource(Task.getPriorityIcon(task.priority))
        binding.taskDetailDeadline.text = task.deadline
        DrawableCompat.setTint(
            binding.taskDetailPriority.drawable,
            ContextCompat.getColor(
                binding.root.context,
                Task.getPriorityIconColor(task.priority)
            )
        )
        viewModel.changeStatus(task.status)
    }

    private fun setupTaskDetailStatusText() {
        binding.taskDetailStatus.text = when (viewModel.getStatus()) {
            TaskStatus.TODO -> getString(R.string.task_status_todo)
            TaskStatus.IN_PROGRESS -> getString(R.string.task_status_in_progress)
            TaskStatus.DONE -> getString(R.string.task_status_done)
        }
    }

    private fun goToUpdateTask(task: Task) {
        val action = TaskDetailFragmentDirections.actionCreateTask(task.id, task.groupId)
        binding.root.findNavController().navigate(action)
    }


    private fun addComment() {
        val comment = binding.addCommentEditText.text.toString()
        if (comment.isNotEmpty()) {
            viewModel.addComment(comment)
            lifecycleScope.launch { repository.updateTask(task) }
        }
    }

    private fun removeCommentEditTextContent() {
        binding.addCommentEditText.setText("")
    }

    private fun addCommentClickListener() {
        binding.sendCommentButton.setOnClickListener {
            addComment()
            removeCommentEditTextContent()
        }
    }

    override fun showRemoveDialogTask(view: View, position: Int) {
        val dialogTitle = view.context.getString(R.string.remove_comment_dialog_title)
        val dialogMessage = view.context.getString(R.string.remove_comment_dialog_message)
        val positive = view.context.getString(R.string.remove_comment_dialog_positive_button)
        val negative = view.context.getString(R.string.dialog_negative_button)
        val myDialog = AlertDialog.Builder(view.context)

        myDialog
            .setTitle(dialogTitle)
            .setMessage(dialogMessage)
            .setPositiveButton(positive) { dialog, _ ->
                removeComment(position)
                dialog.dismiss()
            }
            .setNegativeButton(negative) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .show()
    }

    private fun removeComment(position: Int) {
        viewModel.removeComment(position)
        lifecycleScope.launch { repository.updateTask(task) }
    }
}
