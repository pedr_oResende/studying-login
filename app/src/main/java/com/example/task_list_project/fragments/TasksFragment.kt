package com.example.task_list_project.fragments

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R
import com.example.task_list_project.databinding.FragmentTasksBinding
import com.example.task_list_project.model.Task
import com.example.task_list_project.recyclerView.TaskListAdapter
import com.example.task_list_project.utils.MyDialog
import com.example.task_list_project.viewmodels.TasksViewModel
import kotlinx.coroutines.launch


class TasksFragment : Fragment(), TaskListAdapter.ItemClickListener {

    private var _binding: FragmentTasksBinding? = null
    private val binding get() = _binding!!

    private lateinit var taskListRecyclerView: RecyclerView
    private val viewModel: TasksViewModel by viewModels()

    private var groupId: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTasksBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.taskList.observe(viewLifecycleOwner, {
            lifecycleScope.launch {
                updateRecyclerView(viewModel.getUserTasks(groupId))
            }
        })

        setupArgs()
        setupLayoutIfArgs()
        onFabPressed()
        setupRecyclerView()
        setupToolbar()
    }

    private fun setupArgs() {
        arguments?.let {
            groupId = TasksFragmentArgs.fromBundle(it).groupId
        }
    }

    private fun setupLayoutIfArgs() {
        if (groupId.isNotEmpty() && groupId != "0") {
            lifecycleScope.launch {
                binding.floatingActionButton.isVisible = viewModel.checkUserIsGroupLeader(groupId)
            }
        }
    }

    private fun onFabPressed() {
        binding.floatingActionButton.setOnClickListener {
            val action = TasksFragmentDirections.actionCreateTask(groupId = groupId, taskId = "")
            binding.root.findNavController().navigate(action)
        }
    }

    override fun goToTaskDetail(task: Task) {
        lifecycleScope.launch {
            val action = TasksFragmentDirections.actionTaskDetail(
                taskId = task.id,
                isEditable = viewModel.checkTaskIsEditable(groupId)
            )
            binding.root.findNavController().navigate(action)
        }
    }

    private fun setupRecyclerView() {
        taskListRecyclerView = binding.taskListRecyclerView
        taskListRecyclerView.layoutManager = LinearLayoutManager(binding.root.context)
        lifecycleScope.launch {
            updateRecyclerView(viewModel.getUserTasks(groupId))
        }
    }

    private fun updateRecyclerView(tasks: List<Task>) {
        updateEmptyListMessage(tasks.isEmpty())
        lifecycleScope.launch {
            taskListRecyclerView.adapter = TaskListAdapter(applyFilters(tasks), this@TasksFragment)
        }
    }

    private fun updateEmptyListMessage(isEmpty: Boolean) {
        binding.emptyMessageTextView.isVisible = isEmpty
    }

    private fun setupToolbar() {
        lifecycleScope.launch { binding.toolbar.title = viewModel.getGroupName(groupId) }
        val requireActivity = (requireActivity() as AppCompatActivity)
        requireActivity.setSupportActionBar(binding.toolbar)
        requireActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        requireActivity.supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_task_list, menu)
        setSearchView()
        updateFiltersOptionsMenu()
        updateGroupDetailMenuVisibility(menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun updateGroupDetailMenuVisibility(menu: Menu) {
        menu.findItem(R.id.menu_group_detail).isVisible = groupId != "0"
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_filter -> {
                showApplyFilterDialog(
                    MyDialog.getDialogLayout(
                        requireActivity().layoutInflater,
                        R.layout.filter_task_dialog
                    )
                )
                true
            }
            R.id.menu_search -> {
                true
            }
            R.id.menu_group_detail -> {
                goToGroupDetail()
                true
            }
            R.id.menu_clear_filter -> {
                viewModel.cleanFilters()
                updateFiltersOptionsMenu()
                true
            }
            android.R.id.home -> {
                binding.root.findNavController().navigateUp()
                true
            }
            else -> onOptionsItemSelected(item)
        }
    }

    private fun goToGroupDetail() {
        val action = TasksFragmentDirections.actionGroupDetail(groupId)
        binding.root.findNavController().navigate(action)
    }

    private fun setSearchView() {
        val item = binding.toolbar.menu.findItem(R.id.menu_search)
        val searchView = item.actionView as SearchView
        searchView.queryHint = getString(R.string.search_edit_text_hint)
        val searchEditText: EditText = searchView.findViewById(R.id.search_src_text)

        searchEditText.setTextColor(Color.WHITE)
        searchEditText.setHintTextColor(Color.WHITE)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                lifecycleScope.launch {
                    updateRecyclerView(
                        viewModel.filterByTitle(
                            newText,
                            viewModel.getUserTasks(groupId)
                        )
                    )
                }
                return false
            }
        })
    }

    private fun updateFilterLayout(dialogView: View, myFilter: String) {
        val filters = resources.getStringArray(R.array.filter_options)

        val priorityContainer: ConstraintLayout =
            dialogView.findViewById(R.id.priorityFilterContainer)
        val statusContainer: ConstraintLayout = dialogView.findViewById(R.id.statusFilterContainer)

        when (myFilter) {
            filters[0] -> { // Priority
                priorityContainer.isVisible = true
                statusContainer.isVisible = false
            }
            filters[1] -> { // Status
                statusContainer.isVisible = true
                priorityContainer.isVisible = false
            }
            filters[2] -> { // Both
                statusContainer.isVisible = true
                priorityContainer.isVisible = true
            }
        }
    }

    private fun applyFilters(tasks: List<Task>): List<Task> {
        return viewModel.filter(tasks)
    }

    private fun showApplyFilterDialog(dialogView: View) {

        val dialogTitle = getString(R.string.filter_dialog_title)
        val positiveButtonTitle = getString(R.string.filter_dialog_positive_button)
        val negativeButtonTitle = getString(R.string.dialog_negative_button)
        val myDialog = AlertDialog.Builder(binding.root.context)
        val filterSpinner = dialogView.findViewById<Spinner>(R.id.filterSpinner)
        MyDialog.setSpinner(filterSpinner, R.array.filter_options, binding.root.context)
        onClickFilterListeners(dialogView)

        viewModel.filters.observe(viewLifecycleOwner, { updateFilterButtonBackground(dialogView) })

        filterSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                updateFilterLayout(dialogView, parent.getItemAtPosition(position).toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}

        }
        myDialog
            .setTitle(dialogTitle)
            .setView(dialogView)
            .setPositiveButton(positiveButtonTitle) { dialog, _ ->
                lifecycleScope.launch {
                    if (viewModel.hasFilters()) {
                        updateRecyclerView(viewModel.getUserTasks(groupId))
                        updateFiltersOptionsMenu()
                    }
                }
                dialog.dismiss()
            }
            .setNegativeButton(negativeButtonTitle) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .show()
    }

    override fun showRemoveDialogTask(view: View, task: Task) {
        val dialogTitle = view.context.getString(R.string.remove_task_dialog_title)
        val dialogMessage = view.context.getString(R.string.remove_task_dialog_message)
        val positive = view.context.getString(R.string.remove_dialog_positive_button)
        val negative = view.context.getString(R.string.dialog_negative_button)
        val myDialog = AlertDialog.Builder(view.context)

        myDialog
            .setTitle(dialogTitle)
            .setMessage(dialogMessage)
            .setPositiveButton(positive) { dialog, _ ->
                lifecycleScope.launch {
                    viewModel.removeTask(task)
                }
                dialog.dismiss()
            }
            .setNegativeButton(negative) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .show()
    }

    private fun updateFiltersOptionsMenu() {
        if (viewModel.hasFilters()) {
            binding.toolbar.menu.findItem(R.id.menu_clear_filter).isVisible = true
            binding.toolbar.menu.findItem(R.id.menu_filter).isVisible = false
        } else {
            binding.toolbar.menu.findItem(R.id.menu_clear_filter).isVisible = false
            binding.toolbar.menu.findItem(R.id.menu_filter).isVisible = true
        }
    }

    private fun updatePriorityButtonBackground(click: Int): Drawable? {
        return if (click == 0) {
            ContextCompat.getDrawable(
                binding.root.context,
                R.drawable.bg_priority_on_create_task
            )
        } else {
            ContextCompat.getDrawable(
                binding.root.context,
                R.drawable.bg_priority_off_create_task
            )
        }
    }

    private fun updateStatusButtonBackground(click: Int): Int {
        return if (click == 0) {
            ContextCompat.getColor(
                binding.root.context,
                R.color.primaryColor_variant
            )
        } else {
            ContextCompat.getColor(
                binding.root.context,
                R.color.primaryColor
            )
        }
    }

    private fun updateFilterButtonBackground(dialogView: View) {
        val lowPriority = dialogView.findViewById<ImageView>(R.id.buttonPriorityLow)
        val mediumPriority = dialogView.findViewById<ImageView>(R.id.buttonPriorityMedium)
        val highPriority = dialogView.findViewById<ImageView>(R.id.buttonPriorityHigh)

        val toDoStatus = dialogView.findViewById<Button>(R.id.buttonStatusToDo)
        val inProgressStatus = dialogView.findViewById<Button>(R.id.buttonStatusInProgress)
        val doneStatus = dialogView.findViewById<Button>(R.id.buttonStatusDone)

        if (viewModel.getFilters()[0]) {
            lowPriority.background = updatePriorityButtonBackground(0)
        } else {
            lowPriority.background = updatePriorityButtonBackground(1)
        }
        if (viewModel.getFilters()[1]) {
            mediumPriority.background = updatePriorityButtonBackground(0)
        } else {
            mediumPriority.background = updatePriorityButtonBackground(1)
        }
        if (viewModel.getFilters()[2]) {
            highPriority.background = updatePriorityButtonBackground(0)
        } else {
            highPriority.background = updatePriorityButtonBackground(1)
        }

        if (viewModel.getFilters()[3]) {
            toDoStatus.setBackgroundColor(updateStatusButtonBackground(0))
        } else {
            toDoStatus.setBackgroundColor(updateStatusButtonBackground(1))
        }
        if (viewModel.getFilters()[4]) {
            inProgressStatus.setBackgroundColor(updateStatusButtonBackground(0))
        } else {
            inProgressStatus.setBackgroundColor(updateStatusButtonBackground(1))
        }
        if (viewModel.getFilters()[5]) {
            doneStatus.setBackgroundColor(updateStatusButtonBackground(0))
        } else {
            doneStatus.setBackgroundColor(updateStatusButtonBackground(1))
        }
    }

    private fun onClickFilterListeners(dialogView: View) {
        dialogView.findViewById<ImageView>(R.id.buttonPriorityLow).setOnClickListener {
            viewModel.updateFilters(0)
        }
        dialogView.findViewById<ImageView>(R.id.buttonPriorityMedium).setOnClickListener {
            viewModel.updateFilters(1)
        }
        dialogView.findViewById<ImageView>(R.id.buttonPriorityHigh).setOnClickListener {
            viewModel.updateFilters(2)
        }
        dialogView.findViewById<Button>(R.id.buttonStatusToDo).setOnClickListener {
            viewModel.updateFilters(3)
        }
        dialogView.findViewById<Button>(R.id.buttonStatusInProgress).setOnClickListener {
            viewModel.updateFilters(4)
        }
        dialogView.findViewById<Button>(R.id.buttonStatusDone).setOnClickListener {
            viewModel.updateFilters(5)
        }
    }

}