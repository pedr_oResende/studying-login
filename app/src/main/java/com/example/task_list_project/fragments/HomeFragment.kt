package com.example.task_list_project.fragments

import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R
import com.example.task_list_project.databinding.FragmentHomeBinding
import com.example.task_list_project.model.Group
import com.example.task_list_project.recyclerView.GroupAdapter
import com.example.task_list_project.viewmodels.GroupsViewModel
import kotlinx.coroutines.launch


class HomeFragment : Fragment(), GroupAdapter.HomeFragmentListener {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var groupsRecyclerView: RecyclerView
    private val viewModel: GroupsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isUserLogged()
        setupToolbar()
        setupViewModel()
        setupRecyclerView()
        onFabPressed()
    }

    private fun isUserLogged() {
        lifecycleScope.launch {
            if (!viewModel.isUserLogged()) {
                goToLogin(binding.root)
            }
        }
    }

    private fun goToLogin(view: View) {
        view.findNavController().navigate(R.id.actionLogin)
    }

    private fun goToProfile(view: View) {
        lifecycleScope.launch {
            val action = HomeFragmentDirections.actionProfile(viewModel.getOnlineUser()!!.id)
            view.findNavController().navigate(action)
        }
    }

    private fun setupViewModel() {
        viewModel.groups.observe(viewLifecycleOwner, {
            lifecycleScope.launch {
                updateRecyclerView(viewModel.getGroupsByUser(viewModel.getOnlineUser()))
            }
        })
    }

    private fun onFabPressed() {
        binding.floatingActionButton.setOnClickListener {
            goToCreateGroup()
        }
    }

    private fun setupRecyclerView() {
        groupsRecyclerView = binding.groupsRecyclerView
        groupsRecyclerView.layoutManager = GridLayoutManager(binding.root.context, 2)
        lifecycleScope.launch {
            updateRecyclerView(viewModel.getGroupsByUser(viewModel.getOnlineUser()))
        }
    }

    private fun updateRecyclerView(groups: List<Group>) {
        lifecycleScope.launch {
            groupsRecyclerView.adapter = GroupAdapter(groups, this@HomeFragment)
        }
    }

    private fun setupToolbar() {
        val requireActivity = (requireActivity() as AppCompatActivity)
        requireActivity.setSupportActionBar(binding.toolbar)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_home, menu)
        setSearchView()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_profile -> {
                goToProfile(binding.root)
                true
            }
            R.id.menu_search -> {
                true
            }
            else -> onOptionsItemSelected(item)
        }
    }

    private fun setSearchView() {
        val item = binding.toolbar.menu.findItem(R.id.menu_search)
        val searchView = item.actionView as SearchView
        searchView.queryHint = getString(R.string.search_edit_text_hint)
        val searchEditText: EditText = searchView.findViewById(R.id.search_src_text)

        searchEditText.setTextColor(Color.WHITE)
        searchEditText.setHintTextColor(Color.WHITE)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                lifecycleScope.launch {
                    updateRecyclerView(viewModel.filterByTitle(newText, viewModel.getOnlineUser()))
                }
                return false
            }
        })
    }

    override fun goToTasks(groupId: String) {
        val action = HomeFragmentDirections.actionTasks(groupId)
        binding.root.findNavController().navigate(action)
    }

    override fun showRemoveDialog(view: View, group: Group) {
        val dialogTitle = view.context.getString(R.string.remove_group_dialog_title)
        val dialogMessage = view.context.getString(R.string.remove_group_dialog_message)
        val positive = view.context.getString(R.string.remove_dialog_positive_button)
        val negative = view.context.getString(R.string.dialog_negative_button)
        val myDialog = AlertDialog.Builder(view.context)

        myDialog
            .setTitle(dialogTitle)
            .setMessage(dialogMessage)
            .setPositiveButton(positive) { dialog, _ ->
                lifecycleScope.launch {
                    viewModel.removeGroup(group)
                }
                dialog.dismiss()
            }
            .setNegativeButton(negative) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .show()
    }

    private fun goToCreateGroup() {
        val action = HomeFragmentDirections.actionCreateGroup("", "")
        binding.root.findNavController().navigate(action)
    }
}