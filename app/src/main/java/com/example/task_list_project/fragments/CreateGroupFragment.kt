package com.example.task_list_project.fragments

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R
import com.example.task_list_project.databinding.FragmentCreateGroupBinding
import com.example.task_list_project.recyclerView.MembersInCreateGroupAdapter
import com.example.task_list_project.viewmodels.CreateGroupViewModel
import kotlinx.coroutines.launch

class CreateGroupFragment : Fragment(), MembersInCreateGroupAdapter.ItemClickListener {

    private var _binding: FragmentCreateGroupBinding? = null
    private val binding get() = _binding!!

    private val viewModel: CreateGroupViewModel by viewModels()
    private lateinit var namesRecyclerView: RecyclerView
    private var groupId = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCreateGroupBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.members.observe(viewLifecycleOwner, { updateRecyclerView() })
        setupToolBar()
        setFieldsIfArgs()
        setupRecyclerView()
        addMemberClickListener()
    }

    private fun setupRecyclerView() {
        namesRecyclerView = binding.membersListLayout.membersList
        namesRecyclerView.layoutManager = LinearLayoutManager(
            binding.root.context,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        lifecycleScope.launch {
            viewModel.addMemberToList(viewModel.getOnlineUser()!!.email)
            updateRecyclerView()
        }
    }

    private fun addMemberClickListener() {
        binding.addMemberButton.setOnClickListener {
            addToGroup()
            clearMemberEditText()
        }
    }

    private fun clearMemberEditText() {
        binding.addMemberEditText.setText("")
    }

    private fun addToGroup() {
        lifecycleScope.launch {
            val message =
                if (viewModel.addMemberToList(binding.addMemberEditText.text.toString())) {
                    getString(R.string.add_user_to_group_success)
                } else {
                    getString(R.string.add_user_to_group_fail)
                }
            Toast.makeText(
                binding.root.context,
                message,
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun updateRecyclerView() {
        lifecycleScope.launch {
            namesRecyclerView.adapter = MembersInCreateGroupAdapter(
                viewModel.getNames(),
                this@CreateGroupFragment
            )
        }
    }

    private fun setFieldsIfArgs() {
        arguments?.let {
            lifecycleScope.launch {
                groupId = CreateGroupFragmentArgs.fromBundle(it).groupId
                val userId = CreateGroupFragmentArgs.fromBundle(it).userId
                if (groupId.isNotEmpty()) {
                    val group = viewModel.getGroupById(groupId)
                    binding.groupName.setText(group.name)
                    binding.groupDescription.setText(group.description)
                    if (userId == group.leaderId) {
                        viewModel.setupMembers(group.membersId)
                    } else {
                        binding.addMembersContainer.isVisible = false
                    }
                }
            }
        }
    }

    override fun showRemoveDialog(view: View, position: Int) {
        val dialogTitle = view.context.getString(R.string.remove_member_dialog_title)
        val dialogMessage = view.context.getString(R.string.remove_member_dialog_message)
        val positive = view.context.getString(R.string.remove_dialog_positive_button)
        val negative = view.context.getString(R.string.dialog_negative_button)
        val myDialog = AlertDialog.Builder(view.context)

        myDialog
            .setTitle(dialogTitle)
            .setMessage(dialogMessage)
            .setPositiveButton(positive) { dialog, _ ->
                lifecycleScope.launch {
                    viewModel.removeMember(position, groupId)
                }
                dialog.dismiss()
            }
            .setNegativeButton(negative) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .show()
    }

    override fun userIsLeader(): Boolean {
        return true
    }

    private fun setupToolBar() {
        val requireActivity = (requireActivity() as AppCompatActivity)
        requireActivity.setSupportActionBar(binding.toolbar)
        requireActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        requireActivity.supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun updateOptionsMenuIfArgs() {
        val menu = binding.toolbar.menu

        arguments?.let {
            if (CreateGroupFragmentArgs.fromBundle(it).groupId.isEmpty()) {
                menu.findItem(R.id.menu_create_group).isVisible = true
            } else {
                menu.findItem(R.id.menu_update_group).isVisible = true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_create_group, menu)
        updateOptionsMenuIfArgs()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onCancelGroup()
                true
            }
            R.id.menu_create_group -> {
                onCreateGroup()
                true
            }
            R.id.menu_update_group -> {
                backToHome()
                true
            }
            else -> onOptionsItemSelected(item)
        }
    }

    private fun onCancelGroup() {
        backToHome()
    }

    private fun onCreateGroup() {
        val name = binding.groupName.text.toString()
        val description = binding.groupDescription.text.toString()
        lifecycleScope.launch {
            if (viewModel.createGroup(name, description)) {
                backToHome()
            } else {
                Toast.makeText(
                    binding.root.context,
                    getString(R.string.toast_create_group_failed),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun backToHome() {
        binding.root.findNavController().navigateUp()
    }

}