package com.example.task_list_project.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.example.task_list_project.R
import com.example.task_list_project.activities.MainActivity
import com.example.task_list_project.databinding.FragmentSignUpBinding
import com.example.task_list_project.model.User
import com.example.task_list_project.viewmodels.GroupsViewModel
import kotlinx.coroutines.launch
import org.json.JSONArray

class SignUpFragment : Fragment() {

    private var _binding: FragmentSignUpBinding? = null
    private val binding get() = _binding!!

    private val repository by lazy { MainActivity.repository }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSignUpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolBar()
        onSignUpCLicked()
    }

    private fun setupToolBar() {
        val requireActivity = (requireActivity() as AppCompatActivity)

        requireActivity.setSupportActionBar(binding.toolbar)
        requireActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        requireActivity.supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun onSignUpCLicked() {
        binding.signUp.setOnClickListener {
            addUser(it)
        }
    }

    private fun addUser(view: View) {
        val username = binding.usernameEditText.text.toString()
        val email = binding.emailEditText.text.toString()
        val password = binding.passwordEditText.text.toString()
        val user = User(
            username = username,
            email = email,
            password = password
        )
        lifecycleScope.launch {
            if (isValidForm(user)) {
                setupInitialGroups(user)
                repository.addUser(user)
                Toast.makeText(
                    binding.root.context,
                    getString(R.string.toast_user_created),
                    Toast.LENGTH_SHORT
                ).show()
                view.findNavController().navigateUp()
            }
        }
    }

    private fun setupInitialGroups(user: User) {
        user.groupsId = JSONArray(arrayListOf(GroupsViewModel.ID_NOTES)).toString()
    }

    private suspend fun isValidForm(user: User): Boolean {
        return when {
            !isUserValid(user) -> {
                Toast.makeText(
                    binding.root.context,
                    getString(R.string.toast_sign_in_failed),
                    Toast.LENGTH_LONG
                ).show()
                false
            }
            userExists(user) -> {
                Toast.makeText(
                    binding.root.context,
                    getString(R.string.toast_user_exists),
                    Toast.LENGTH_LONG
                ).show()
                false
            }
            else -> true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            android.R.id.home -> {
                binding.root.findNavController().navigateUp()
                true
            }
            else -> onOptionsItemSelected(item)
        }
    }

    private fun isUserValid(user: User): Boolean {
        return user.isPasswordValid() && user.isUsernameValid() && user.isEmailValid()
    }

    private suspend fun userExists(user: User): Boolean {
        return repository.getUser(user.email) != null
    }


}