package com.example.task_list_project.fragments

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R
import com.example.task_list_project.databinding.FragmentProfileBinding
import com.example.task_list_project.recyclerView.GroupsInProfileAdapter
import com.example.task_list_project.viewmodels.ProfileViewModel
import kotlinx.coroutines.launch

class ProfileFragment : Fragment(), GroupsInProfileAdapter.ProfileFragmentListener {


    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private var userId = ""
    private lateinit var groupsRecyclerView: RecyclerView
    private val viewModel: ProfileViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch {
            setupFieldsWithArgs()
            setupRecyclerView()
        }
        setupToolBar()
        onLogout()
    }


    private fun setupToolBar() {
        val requireActivity = (requireActivity() as AppCompatActivity)
        requireActivity.setSupportActionBar(binding.toolbar)
        requireActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        requireActivity.supportActionBar?.setDisplayShowHomeEnabled(true)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_create_task, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                binding.root.findNavController().navigateUp()
                true
            }
            else -> onOptionsItemSelected(item)
        }
    }


    private suspend fun setupFieldsWithArgs() {
        arguments?.let {
            userId = ProfileFragmentArgs.fromBundle(it).userId
            val user = viewModel.getUserById(userId)
            binding.usernameProfile.text = user.username
        }
    }

    private fun setupRecyclerView() {
        groupsRecyclerView = binding.membersListLayout.membersList
        groupsRecyclerView.layoutManager = LinearLayoutManager(
            binding.root.context,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        updateRecyclerView()
    }

    private fun updateRecyclerView() {
        lifecycleScope.launch {
            groupsRecyclerView.adapter = GroupsInProfileAdapter(
                groups = viewModel.getGroupsByUser(viewModel.getOnlineUser()),
                listener = this@ProfileFragment
            )
        }
    }

    override fun goToTasks(groupId: String) {
        val action = ProfileFragmentDirections.actionTasks(groupId)
        binding.root.findNavController().navigate(action)
    }

    private fun onLogout() {
       binding.logoutButton.setOnClickListener {
           lifecycleScope.launch {
               viewModel.logoutUser()
               goToLogin(binding.root)
           }
       }
    }

    private fun goToLogin(view: View) {
        view.findNavController().navigate(R.id.actionLogin)
    }
}