package com.example.task_list_project.fragments

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R
import com.example.task_list_project.databinding.FragmentGroupDetailBinding
import com.example.task_list_project.model.Group
import com.example.task_list_project.model.Task
import com.example.task_list_project.recyclerView.MembersInCreateGroupAdapter
import com.example.task_list_project.recyclerView.MembersInGroupDetailAdapter
import com.example.task_list_project.recyclerView.TaskListAdapter
import com.example.task_list_project.viewmodels.CreateGroupViewModel
import com.example.task_list_project.viewmodels.GroupDetailViewModel
import com.example.task_list_project.viewmodels.TasksViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class GroupDetailFragment :
    Fragment(),
    MembersInCreateGroupAdapter.ItemClickListener,
    TaskListAdapter.ItemClickListener,
    MembersInGroupDetailAdapter.ItemClickListener {

    private var _binding: FragmentGroupDetailBinding? = null
    private val binding get() = _binding!!

    private var groupId = ""
    private lateinit var membersRecyclerView: RecyclerView
    private lateinit var tasksRecyclerView: RecyclerView
    private val viewModel: GroupDetailViewModel by viewModels()
    private val membersViewModel: CreateGroupViewModel by viewModels()
    private val tasksViewModel: TasksViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentGroupDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolBar()
        membersViewModel.members.observe(viewLifecycleOwner, {
            lifecycleScope.launch {
                updateMembersList(viewModel.getMemberPosition())
            }
        })
        setupArgs()
        setupLayout()
        addMemberClickListener()
        leaveGroupClickListener()
        viewModel.filter.observe(viewLifecycleOwner, {
            changeFilterMenu()
        })
    }

    private fun setupArgs() {
        arguments?.let {
            lifecycleScope.launch {
                groupId = GroupDetailFragmentArgs.fromBundle(it).groupId
            }
        }
    }

    private fun setupLayout() {
        lifecycleScope.launch {
            val group = viewModel.getGroupById(groupId)
            changeUserView(group)
            binding.toolbar.title = group.name
            binding.groupDescription.text = group.description
            setupMembersList()
            setupTaskList()
        }
    }

    private fun changeUserView(group: Group) {
        lifecycleScope.launch {
            val userId = viewModel.getOnlineUser()!!.id
            if (group.leaderId == userId) {
                binding.addMembersContainer.isVisible = true
            } else {
                binding.leaveGroupButton.isVisible = true
            }
        }
    }

    private fun setupMembersList() {
        membersRecyclerView = binding.membersListLayout.membersList
        membersRecyclerView.layoutManager = LinearLayoutManager(
            binding.root.context,
            LinearLayoutManager.HORIZONTAL,
            false
        )
    }


    private fun addMemberClickListener() {
        binding.addMemberButton.setOnClickListener {
            addToGroup()
            clearMemberEditText()
        }
    }

    private fun leaveGroupClickListener() {
        binding.leaveGroupButton.setOnClickListener {
            leaveGroup()
            binding.root.findNavController().navigate(R.id.actionHome)
        }
    }

    private fun leaveGroup() {
        lifecycleScope.launch {
            membersViewModel.removeMember(groupId)
        }
    }

    private fun clearMemberEditText() {
        binding.addMemberEditText.setText("")
    }

    private fun addToGroup() {
        lifecycleScope.launch {
            val message =
                if (membersViewModel.addMemberToGroup(
                        binding.addMemberEditText.text.toString(),
                        groupId
                    )
                ) {
                    getString(R.string.add_user_to_group_success)
                } else {
                    getString(R.string.add_user_to_group_fail)
                }
            Toast.makeText(
                binding.root.context,
                message,
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private suspend fun updateMembersList(position: Int) {
        val group = viewModel.getGroupById(groupId)
        membersViewModel.setupMembers(group.membersId)
        membersRecyclerView.adapter = MembersInGroupDetailAdapter(
            membersViewModel.getNames(),
            this@GroupDetailFragment,
            position
        )
    }

    private suspend fun setupTaskList() {
        tasksRecyclerView = binding.groupTaskList
        tasksRecyclerView.layoutManager = LinearLayoutManager(binding.root.context)
        updateTaskList(tasksViewModel.getGroupTasks(groupId))
    }

    private fun updateTaskList(taskList: List<Task>) {
        tasksRecyclerView.adapter =
            TaskListAdapter(taskList, this@GroupDetailFragment)
    }

    private fun setupToolBar() {
        val requireActivity = (requireActivity() as AppCompatActivity)
        requireActivity.setSupportActionBar(binding.toolbar)
        requireActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        requireActivity.supportActionBar?.setDisplayShowHomeEnabled(true)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_group_detail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                binding.root.findNavController().navigateUp()
            }
            R.id.menu_filter_on_group_tasks -> {
                changeFilter()
                true
            }
            R.id.menu_filter_off_group_tasks -> {
                changeFilter()
                resetFilter()
                true
            }
            else -> onOptionsItemSelected(item)
        }
    }

    private fun changeFilterMenu() {
        val filterOn = binding.toolbar.menu.findItem(R.id.menu_filter_on_group_tasks)
        val filterOff = binding.toolbar.menu.findItem(R.id.menu_filter_off_group_tasks)

        if (filterOn != null) {
            val temp = filterOn.isVisible
            filterOn.isVisible = filterOff.isVisible
            filterOff.isVisible = temp
        }
    }

    override fun showRemoveDialog(view: View, position: Int) {
        val dialogTitle = view.context.getString(R.string.remove_member_dialog_title)
        val dialogMessage = view.context.getString(R.string.remove_member_dialog_message)
        val positive = view.context.getString(R.string.remove_dialog_positive_button)
        val negative = view.context.getString(R.string.dialog_negative_button)
        val myDialog = AlertDialog.Builder(view.context)

        myDialog
            .setTitle(dialogTitle)
            .setMessage(dialogMessage)
            .setPositiveButton(positive) { dialog, _ ->
                lifecycleScope.launch {
                    membersViewModel.removeMember(position, groupId)
                }
                dialog.dismiss()
            }
            .setNegativeButton(negative) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .show()
    }

    override fun userIsLeader(): Boolean {
        var isLeader = false
        runBlocking {
            val job = async { checkUserIsLeaderJob() }
            runBlocking {
                isLeader = job.await()
            }
        }
        return isLeader
    }

    private suspend fun checkUserIsLeaderJob(): Boolean {
        val userId = viewModel.getOnlineUser()?.id ?: ""
        val group = viewModel.getGroupById(groupId)
        return userId == group.leaderId
    }

    override fun showRemoveDialogTask(view: View, task: Task) {
        val dialogTitle = view.context.getString(R.string.remove_task_dialog_title)
        val dialogMessage = view.context.getString(R.string.remove_task_dialog_message)
        val positive = view.context.getString(R.string.remove_dialog_positive_button)
        val negative = view.context.getString(R.string.dialog_negative_button)
        val myDialog = AlertDialog.Builder(view.context)

        myDialog
            .setTitle(dialogTitle)
            .setMessage(dialogMessage)
            .setPositiveButton(positive) { dialog, _ ->
                lifecycleScope.launch {
                    tasksViewModel.removeTask(task)
                }
                dialog.dismiss()
            }
            .setNegativeButton(negative) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .show()
    }

    override fun goToTaskDetail(task: Task) {
        lifecycleScope.launch {
            val action = TasksFragmentDirections.actionTaskDetail(
                taskId = task.id,
                isEditable = tasksViewModel.checkTaskIsEditable(groupId)
            )
            binding.root.findNavController().navigate(action)
        }
    }

    override fun filterOnClick(position: Int) {
        lifecycleScope.launch {
            if (viewModel.getFilter()) {
                updateMembersList(position)
                updateTaskList(
                    viewModel.getTasksByUser(
                        viewModel.getUserIdByPosition(position, groupId),
                        tasksViewModel.getGroupTasks(groupId)
                    )
                )
            }
        }
    }
    
    private fun changeFilter() {
        viewModel.changeFilter()
    }

    private fun resetFilter() {
        lifecycleScope.launch {
            updateMembersList(-1)
            updateTaskList(viewModel.getTasksByGroup(groupId))
        }
    }
}