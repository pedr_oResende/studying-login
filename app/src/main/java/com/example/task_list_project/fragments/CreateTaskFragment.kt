package com.example.task_list_project.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R
import com.example.task_list_project.databinding.FragmentCreateTaskBinding
import com.example.task_list_project.model.Task
import com.example.task_list_project.recyclerView.MembersInCreateTaskAdapter
import com.example.task_list_project.viewmodels.CreateTaskViewModel
import kotlinx.coroutines.launch
import java.util.*

class CreateTaskFragment : Fragment(), MembersInCreateTaskAdapter.ItemClickListener {

    private var _binding: FragmentCreateTaskBinding? = null
    private val binding get() = _binding!!

    private val viewModel: CreateTaskViewModel by viewModels()
    private lateinit var membersRecyclerView: RecyclerView
    private lateinit var task: Task
    private lateinit var groupId: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCreateTaskBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch {
            setFieldsIfArgs()
            setupRecyclerView()
            viewModel.memberPosition.observe(viewLifecycleOwner, {
                updateRecyclerView(viewModel.getMemberPosition())
            })
        }
        viewModel.priority.observe(viewLifecycleOwner, { updatePriorityButtonBackground() })
        setupToolBar()
        priorityButtonsListeners()
    }

    private suspend fun setupRecyclerView() {
        if (viewModel.userIsLeader(groupId)) {
            binding.assignTaskContainer.isVisible = true
            membersRecyclerView = binding.membersListLayout.membersList
            membersRecyclerView.layoutManager = LinearLayoutManager(
                binding.root.context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            updateRecyclerView(-1)
        }
    }

    private fun updateRecyclerView(position: Int) {
        lifecycleScope.launch {
            if (viewModel.userIsLeader(groupId)) {
                membersRecyclerView.adapter = MembersInCreateTaskAdapter(
                    viewModel.getMembersName(groupId),
                    this@CreateTaskFragment,
                    position
                )
            }
        }
    }


    override fun onItemClickListener(position: Int) {
        viewModel.updateMemberPosition(position)
    }

    private fun priorityButtonsListeners() {
        binding.buttonPriorityLow.setOnClickListener {
            viewModel.setPriority(1)
        }
        binding.buttonPriorityMedium.setOnClickListener {
            viewModel.setPriority(2)
        }
        binding.buttonPriorityHigh.setOnClickListener {
            viewModel.setPriority(3)
        }
        binding.taskCalendar.setOnClickListener {
            showCalendarDialog()
        }
    }

    private fun showCalendarDialog() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        DatePickerDialog(
            binding.root.context, { _, calendarYear, calendarMonth, calendarDay ->
                var date = "$calendarDay/${calendarMonth + 1}/$calendarYear"
                if (calendarDay < 10)
                    date = "0$date"
                binding.taskCalendar.setText(date)
            }, year, month, day
        ).show()
    }

    private fun updatePriorityButtonBackground() {
        resetAllButtonsBackground()
        when (viewModel.getPriority()) {
            1 -> {
                binding.buttonPriorityLow.background = ContextCompat.getDrawable(
                    binding.root.context,
                    R.drawable.bg_priority_on_create_task
                )
            }
            2 -> {
                binding.buttonPriorityMedium.background = ContextCompat.getDrawable(
                    binding.root.context,
                    R.drawable.bg_priority_on_create_task
                )
            }
            3 -> {
                binding.buttonPriorityHigh.background = ContextCompat.getDrawable(
                    binding.root.context,
                    R.drawable.bg_priority_on_create_task
                )
            }
        }
    }

    private fun resetAllButtonsBackground() {
        binding.buttonPriorityLow.background = ContextCompat.getDrawable(
            binding.root.context,
            R.drawable.bg_priority_off_create_task
        )
        binding.buttonPriorityMedium.background = ContextCompat.getDrawable(
            binding.root.context,
            R.drawable.bg_priority_off_create_task
        )
        binding.buttonPriorityHigh.background = ContextCompat.getDrawable(
            binding.root.context,
            R.drawable.bg_priority_off_create_task
        )
    }

    private suspend fun setFieldsIfArgs() {
        arguments?.let {
            val taskId = CreateTaskFragmentArgs.fromBundle(it).taskId
            groupId = CreateTaskFragmentArgs.fromBundle(it).groupId
            if (taskId.isNotEmpty()) {
                task = viewModel.getTaskById(taskId)
                binding.taskTitle.setText(task.title)
                binding.taskContent.setText(task.content)
                binding.taskCalendar.setText(task.deadline)
                viewModel.setPriority(task.priority)
            }
        }
    }

    private suspend fun generateNewTask(): Task {
        val titleEditText = binding.taskTitle
        val contentEditText = binding.taskContent
        val calendar = binding.taskCalendar

        return viewModel.getTask(
            title = titleEditText.text.toString(),
            content = contentEditText.text.toString(),
            deadline = calendar.text.toString(),
            groupId = groupId
        )
    }

    private fun backToHome() {
        binding.root.findNavController().navigateUp()
    }

    private fun onCancelTask() {
        backToHome()
    }


    private fun onUpdateTask() {
        lifecycleScope.launch {
            if (checkTask(task)) {
                val newTask = generateNewTask()
                viewModel.updateTask(task, newTask)
                backToHome()
            } else {
                Toast.makeText(
                    binding.root.context,
                    getString(R.string.invalid_task_toast),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun onCreateTask() {
        lifecycleScope.launch {
            val task = generateNewTask()
            if (checkTask(task)) {
                viewModel.addTask(task)
                backToHome()
            } else {
                Toast.makeText(
                    binding.root.context,
                    getString(R.string.invalid_task_toast),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun setupToolBar() {
        val requireActivity = (requireActivity() as AppCompatActivity)
        requireActivity.setSupportActionBar(binding.toolbar)
        requireActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        requireActivity.supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun updateOptionsMenuIfArgs() {
        val menu = binding.toolbar.menu

        arguments?.let {
            if (CreateTaskFragmentArgs.fromBundle(it).taskId.isEmpty()) {
                menu.findItem(R.id.menu_create_task).isVisible = true
            } else {
                menu.findItem(R.id.menu_update_task).isVisible = true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_create_task, menu)
        updateOptionsMenuIfArgs()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onCancelTask()
                true
            }
            R.id.menu_create_task -> {
                onCreateTask()
                true
            }
            R.id.menu_update_task -> {
                onUpdateTask()
                true
            }
            else -> onOptionsItemSelected(item)
        }
    }

    private fun checkTask(task: Task): Boolean =
        task.isContentValid() && task.isTitleValid() && task.isDeadlineValid()

}
