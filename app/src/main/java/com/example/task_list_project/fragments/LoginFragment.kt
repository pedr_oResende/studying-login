package com.example.task_list_project.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.example.task_list_project.R
import com.example.task_list_project.databinding.FragmentLoginBinding
import com.example.task_list_project.model.Pair
import com.example.task_list_project.model.User
import com.example.task_list_project.viewmodels.LoginFragmentViewModel
import kotlinx.coroutines.launch


class LoginFragment : Fragment() {


    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val viewModel: LoginFragmentViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.pairs.observe(viewLifecycleOwner, { setupButtons() })
        viewModel.password.observe(viewLifecycleOwner, {
            updatePassword()
            updateLoginButton()
        })

        login()
        signUp()
        setupButtons()
        setupCLickListeners()
    }

    private fun setupCLickListeners() {
        binding.passwordEditText.setOnClickListener {
            setPasswordEditTextState()
        }
        binding.password1.setOnClickListener {
            addDigit(0)
        }
        binding.password2.setOnClickListener {
            addDigit(1)
        }
        binding.password3.setOnClickListener {
            addDigit(2)
        }
        binding.password4.setOnClickListener {
            addDigit(3)
        }
        binding.password5.setOnClickListener {
            addDigit(4)
        }
        binding.backspace.setOnClickListener {
            removeDigit()
        }
    }

    private fun login() {
        binding.login.setOnClickListener {
            doLogin(it)
        }
    }

    private fun signUp() {
        binding.signUpAction.setOnClickListener {
            it.findNavController().navigate(R.id.actionSignUp)
        }
    }

    private fun updatePassword() {
        binding.passwordEditText.setText(viewModel.getPasswordChar())

    }

    private fun updateLoginButton() {
        binding.login.isEnabled = viewModel.getPasswordSize() == 6
    }

    private fun addDigit(position: Int) {
        if (viewModel.getPasswordSize() < 6) {
            val pairs = viewModel.getPairs()
            viewModel.passwordAdd(
                Pair(
                    pairs[position].getFirst(),
                    pairs[position].getSecond()
                )
            )
        } else {
            Toast.makeText(
                binding.root.context,
                getString(R.string.toast_password_limit),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun removeDigit() {
        if (viewModel.getPasswordSize() > 0) {
            viewModel.passwordRemove()
        }
    }

    private fun setupButtons() {
        val pairs = viewModel.getPairs()

        binding.password1.text =
            getString(R.string.password_button_text, pairs[0].getFirst(), pairs[0].getSecond())
        binding.password2.text =
            getString(R.string.password_button_text, pairs[1].getFirst(), pairs[1].getSecond())
        binding.password3.text =
            getString(R.string.password_button_text, pairs[2].getFirst(), pairs[2].getSecond())
        binding.password4.text =
            getString(R.string.password_button_text, pairs[3].getFirst(), pairs[3].getSecond())
        binding.password5.text =
            getString(R.string.password_button_text, pairs[4].getFirst(), pairs[4].getSecond())
    }

    private fun setPasswordEditTextState() {
        if (binding.passwordPairContainer.visibility == View.GONE) {
            binding.passwordPairContainer.visibility = View.VISIBLE
        } else {
            binding.passwordPairContainer.visibility = View.GONE
        }
    }

    private fun doLogin(view: View) {
        lifecycleScope.launch {
            val email = binding.emailEditText.text.toString()
            val user = getUser(email)

            if (isValidForm(user) && user != null) {
                user.isOnline = true
                viewModel.updateUser(user)
                view.findNavController().navigate(R.id.actionHome)
            }
        }
    }

    private fun isValidForm(user: User?): Boolean {

        val password = viewModel.getPassword()

        return when {
            user == null -> {
                Toast.makeText(
                    binding.root.context,
                    getString(R.string.toast_user_do_not_exists),
                    Toast.LENGTH_SHORT
                ).show()
                false
            }
            user.matchPassword(password) -> {
                true
            }
            else -> {
                Toast.makeText(
                    binding.root.context,
                    getString(R.string.toast_login_failed),
                    Toast.LENGTH_SHORT
                ).show()
                false
            }
        }

    }

    private suspend fun getUser(email: String): User? {
        return viewModel.getUser(email)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}