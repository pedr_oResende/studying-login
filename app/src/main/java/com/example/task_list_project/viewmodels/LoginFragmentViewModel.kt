package com.example.task_list_project.viewmodels

import androidx.lifecycle.MutableLiveData
import com.example.task_list_project.model.Pair
import com.example.task_list_project.model.Pairs

class LoginFragmentViewModel : MainViewModel() {

    val pairs = MutableLiveData<ArrayList<Pair>>()
    val password = MutableLiveData<ArrayList<Pair>>()
    private var passwordChar: String

    init {
        pairs.value = Pairs().pairs
        password.value = arrayListOf(Pair())
        password.value!!.removeAt(0)
        passwordChar = ""
    }

    fun passwordAdd(pair: Pair) {
        password.value!!.add(pair)
        addChar()
        password.apply {
            postValue(value)
        }
    }

    fun passwordRemove() {
        password.value!!.removeLast()
        removeChar()
        password.apply {
            postValue(value)
        }
    }

    private fun addChar() {
        passwordChar += "*"
    }

    private fun removeChar() {
        passwordChar = passwordChar.substring(0, passwordChar.length - 1)
    }

    fun getPasswordSize() = password.value?.size ?: 0

    fun getPasswordChar() = passwordChar

    fun getPassword() = password.value!!

    fun getPairs() = pairs.value!!

}