package com.example.task_list_project.viewmodels

import androidx.lifecycle.MutableLiveData
import com.example.task_list_project.model.Group
import com.example.task_list_project.model.User
import kotlinx.coroutines.runBlocking

class CreateGroupViewModel : MainViewModel() {

    val members = MutableLiveData<ArrayList<User>>()

    init {
        members.value = arrayListOf()
    }

    suspend fun addMemberToList(email: String): Boolean {
        val user = repository.getUser(email)
        return if (user != null && !members.value!!.contains(user)) {
            members.apply {
                value?.add(user)
                postValue(value)
            }
            true
        } else {
            false
        }
    }

    suspend fun addMemberToGroup(email: String, groupId: String): Boolean {
        val user = repository.getUser(email)
        return if (user != null && !members.value!!.contains(user)) {
            members.apply {
                val group = repository.getGroupById(groupId)
                val memberList = jsonToArray(group.membersId)
                memberList.add(user.id)
                user.groupsId = addGroupIdInUser(user.groupsId, groupId)
                group.membersId = listToJson(memberList)
                repository.updateUser(user)
                repository.updateGroup(group)
                value?.add(user)
                postValue(value)
            }
            true
        } else {
            false
        }
    }

    suspend fun removeMember(position: Int, groupId: String) {
        members.apply {
            val user = value!![position]
            remove(user, groupId)
            value?.removeAt(position)
            postValue(value)
        }
    }

    suspend fun removeMember(groupId: String) {
        members.apply {
            val user = getOnlineUser()!!
            remove(user, groupId)
            value?.remove(user)
            postValue(value)
        }
    }

    private suspend fun remove(user: User, groupId: String) {
        val group = repository.getGroupById(groupId)
        user.groupsId = removeGroupIdFromUser(user.groupsId, groupId)
        group.membersId = removeUserId(group.membersId!!, user.id)
        repository.updateGroup(group)
        repository.updateUser(user)
    }

    private fun removeUserId(groupMembersId: String, userId: String): String {
        val memberList = jsonToArray(groupMembersId)
        memberList.remove(userId)
        return listToJson(memberList)
    }

    suspend fun setupMembers(membersId: String?) {
        members.apply {
            if (value!!.isEmpty()) {
                value?.addAll(getMembersFromArgs(membersId))
            }
        }
    }

    private suspend fun getMembersFromArgs(membersId: String?): ArrayList<User> {
        if (membersId == null) return arrayListOf()
        val membersIdList: ArrayList<String> = jsonToArray(membersId)
        val usersList = arrayListOf<User>()
        for (memberId in membersIdList) {
            usersList.add(repository.getUserById(memberId))
        }
        return usersList
    }

    suspend fun createGroup(name: String, description: String): Boolean {
        val group = Group(
            leaderId = getOnlineUser()?.id,
            membersId = null,
            name = name,
            description = description
        )
        setMembersGroupId(group.id)
        group.membersId = getMembersId()
        return if (isGroupValid(group)) {
            repository.addGroup(group)
            true
        } else {
            false
        }

    }

    private fun isMembersValid(membersId: String?): Boolean {
        return jsonToArray(membersId).size > 1
    }

    private fun isGroupValid(group: Group): Boolean {
        return group.isNameValid() && isMembersValid(group.membersId)
    }

    private fun setMembersGroupId(groupId: String) {
        members.apply {
            value?.forEach { user ->
                user.groupsId = addGroupIdInUser(user.groupsId, groupId)
                updateUsersInDatabase(user)
            }
        }
    }

    private fun updateUsersInDatabase(user: User) {
        runBlocking {
            repository.updateUser(user)
        }
    }

    private fun getMembersId(): String {
        return listToJson(members.value?.map { it.id } ?: emptyList())
    }

    fun getNames(): List<String> {
        return members.value?.map { it.username } ?: emptyList()
    }

}