package com.example.task_list_project.viewmodels

import androidx.lifecycle.MutableLiveData
import com.example.task_list_project.model.Group
import com.example.task_list_project.model.User

open class GroupsViewModel : MainViewModel() {

    companion object {
        const val ID_NOTES = "0"
    }

    var groups = MutableLiveData<Unit>()

    suspend fun removeGroup(group: Group) {
        removeGroupData(group)
        repository.removeGroup(group)
        groups.apply {
            postValue(value)
        }
    }

    private suspend fun removeGroupData(group: Group) {
        removeTasksWhenRemoveGroup(group)
        updateUserWhenRemoveGroup(group)
    }

    private suspend fun removeTasksWhenRemoveGroup(group: Group) {
        val tasks = getTasksByGroup(group.id)
        tasks.forEach { repository.removeTask(it) }
    }
    private suspend fun updateUserWhenRemoveGroup(group: Group) {
        val members = getUserList(jsonToArray(group.membersId))
        members.forEach { user ->
            removeGroupIdFromUser(user.groupsId, group.id)
            updateUser(user)
        }
    }

    suspend fun isUserLogged(): Boolean {
        return repository.hasOnlineUser()
    }

    suspend fun logoutUser() {
        val user = getOnlineUser()!!
        user.isOnline = false
        updateUser(user)
    }

    suspend fun filterByTitle(name: String, user: User?): List<Group> {
        if (user != null) {
            return getGroupsByUser(user).filter {
                it.name.contains(name, ignoreCase = true)
            }
        }
        return emptyList()
    }
}