package com.example.task_list_project.viewmodels

import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.example.task_list_project.R
import com.example.task_list_project.activities.MainActivity
import com.example.task_list_project.fragments.TasksFragmentDirections
import com.example.task_list_project.model.Group
import com.example.task_list_project.model.Task
import com.example.task_list_project.model.User
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray

open class MainViewModel : ViewModel() {

    val repository by lazy { MainActivity.repository }
    private val gson = GsonBuilder().create()

    fun jsonToArray(string: String?): ArrayList<String> {
        return gson.fromJson(string, object : TypeToken<ArrayList<String>>() {}.type)
    }

    fun listToJson(list: List<String>): String {
        return JSONArray(list).toString()
    }

    suspend fun getUserById(userId: String): User {
        return repository.getUserById(userId)
    }

    suspend fun getUserList(membersId: ArrayList<String>) : ArrayList<User> {
        val userList = arrayListOf<User>()
        membersId.forEach { userList.add(getUserById(it)) }
        return userList
    }

    open suspend fun removeTask(task: Task) {
        repository.removeTask(task)
    }

    suspend fun getGroupsByUser(user: User?): List<Group> {
        return if (user != null) {
            repository.getGroupsByUser(user.groupsId)
        } else
            emptyList()
    }

    suspend fun getGroupById(id: String): Group {
        return repository.getGroupById(id)
    }

    suspend fun getTaskById(id: String): Task {
        return repository.getTaskById(id)!!
    }

    suspend fun getTasksByGroup(groupId: String): List<Task> {
        return repository.getTasksByGroup(groupId)
    }

    suspend fun getOnlineUser(): User? {
        return repository.getOnlineUser()
    }

    fun addGroupIdInUser(userGroupsId: String, groupId: String): String {
        val groupsId: ArrayList<String> = jsonToArray(userGroupsId)
        groupsId.add(groupId)
        return listToJson(groupsId)
    }

    fun removeGroupIdFromUser(userGroupsId: String, groupId: String): String {
        val groupsId: ArrayList<String> = jsonToArray(userGroupsId)
        groupsId.remove(groupId)
        return listToJson(groupsId)
    }


    suspend fun getUser(email: String): User? {
        return repository.getUser(email)
    }

    suspend fun updateUser(user: User) {
        repository.updateUser(user)
    }

}