package com.example.task_list_project.viewmodels

import androidx.lifecycle.MutableLiveData
import com.example.task_list_project.model.Task
import com.example.task_list_project.model.TaskStatus

open class CreateTaskViewModel : MainViewModel() {


    val memberPosition = MutableLiveData<Int>()
    val priority = MutableLiveData<List<Boolean>>()

    init {
        memberPosition.value = -1
        priority.value = listOf(true, false, false)
    }

    private suspend fun getSelectedMemberId(groupId: String): String {
        val group = getGroupById(groupId)
        val members = getUserList(jsonToArray(group.membersId))
        return members[memberPosition.value!!].id
    }

    fun getMemberPosition() = memberPosition.value!!

    fun updateMemberPosition(position: Int) {
        memberPosition.value = position
    }

    private suspend fun getUserId(groupId: String): String {
        return if (memberPosition.value == -1) {
            repository.getOnlineUser()!!.id
        } else {
            getSelectedMemberId(groupId)
        }
    }

    suspend fun getTask(title: String, content: String, deadline: String, groupId: String): Task {
        return Task(
            title = title,
            content = content,
            priority = getPriority(),
            userId = getUserId(groupId),
            status = TaskStatus.TODO,
            deadline = deadline,
            groupId = groupId
        )
    }

    suspend fun addTask(task: Task) {
        repository.addTask(task)
    }
    suspend fun updateTask(task: Task, newTask: Task) {
        task.title = newTask.title
        task.content = newTask.content
        task.priority = newTask.priority
        task.deadline = newTask.deadline
        task.userId = newTask.userId
        repository.updateTask(task)
    }

    fun setPriority(priorityValue: Int) {
        when (priorityValue) {
            1 -> {
                priority.value = listOf(true, false, false)
            }
            2 -> {
                priority.value = listOf(false, true, false)
            }
            3 -> {
                priority.value = listOf(false, false, true)
            }
        }
    }

    fun getPriority(): Int {
        return (priority.value?.indexOf(true) ?: 0) + 1
    }

    suspend fun getMembersName(groupId: String): List<String> {
        val group = getGroupById(groupId)
        return getUserList(jsonToArray(group.membersId)).map { it.username }
    }

    suspend fun userIsLeader(groupId: String): Boolean {
        val group = getGroupById(groupId)
        return repository.getOnlineUser()!!.id == group.leaderId
    }
}