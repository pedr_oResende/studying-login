package com.example.task_list_project.viewmodels

import androidx.lifecycle.MutableLiveData
import com.example.task_list_project.model.Task

class GroupDetailViewModel : CreateTaskViewModel() {

    val filter = MutableLiveData<Boolean>()

    init {
        filter.value = false
    }

    fun getFilter() = filter.value!!

    fun changeFilter() {
        filter.apply {
            value = !getFilter()
        }
    }

    fun getTasksByUser(userId: String, tasks: List<Task>): List<Task> {
        return tasks.filter { it.userId == userId }
    }

    suspend fun getUserIdByPosition(position: Int, groupId: String): String {
        val group = getGroupById(groupId)
        return jsonToArray(group.membersId)[position]
    }
}