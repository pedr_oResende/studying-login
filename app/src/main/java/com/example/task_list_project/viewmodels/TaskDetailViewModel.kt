package com.example.task_list_project.viewmodels

import androidx.lifecycle.MutableLiveData
import com.example.task_list_project.model.TaskStatus

class TaskDetailViewModel : MainViewModel() {

    val status = MutableLiveData<TaskStatus>()
    val comments = MutableLiveData<ArrayList<String>>()

    init {
        status.value = TaskStatus.DONE
        comments.value = arrayListOf()
    }

    fun changeStatus(newStatus: TaskStatus) {
        status.value = newStatus
        status.apply {
            postValue(value)
        }
    }

    fun getStatus() = status.value!!

    fun setComments(commentsJson: String) {
        if (commentsJson.isNotEmpty())
            comments.value = jsonToArray(commentsJson)
    }

    fun getComments(): String = listToJson(comments.value!!)

    fun getCommentsArray(): ArrayList<String> {
        return comments.value ?: arrayListOf()
    }

    fun addComment(comment: String) {
        comments.value?.add(0, comment)
        comments.apply {
            postValue(value)
        }
    }

    fun removeComment(position: Int) {
        comments.value?.removeAt(position)
        comments.apply {
            postValue(value)
        }
    }

}