package com.example.task_list_project.viewmodels

import androidx.lifecycle.MutableLiveData
import com.example.task_list_project.model.Task
import com.example.task_list_project.model.TaskStatus

class TasksViewModel : MainViewModel() {

    val taskList = MutableLiveData<Unit>()
    var filters = MutableLiveData<Array<Boolean>>()

    init {
        filters.value = arrayOf(false, false, false, false, false, false)
    }

    override suspend fun removeTask(task: Task) {
        super.removeTask(task)
        taskList.apply { postValue(value) }

    }

    private suspend fun getTasks(groupId: String): List<Task> {
        val user = getOnlineUser()
        return if (user != null) {
            if (groupId == "0") {
                repository.getTasksByUser(user.id).tasks?.filter { it.groupId == "0" } ?: emptyList()
            } else {
                repository.getTasksByGroup(groupId)
            }
        } else {
            emptyList()
        }
    }

    suspend fun getGroupName(groupId: String): String {
        return repository.getGroupNameById(groupId)
    }

    suspend fun checkUserIsGroupLeader(groupId: String): Boolean {
        val user = getOnlineUser()!!
        return repository.getGroupById(groupId).leaderId == user.id
    }

    suspend fun getGroupTasks(groupId: String): List<Task> {
        return getTasks(groupId).sortedByDescending { it.priority }
    }

    suspend fun getUserTasks(groupId: String): List<Task> {
        return getGroupTasks(groupId).filter { it.userId == getOnlineUser()!!.id }
    }

    fun filterByTitle(title: String, tasks: List<Task>): List<Task> {
        return tasks.filter {
            it.title.contains(title, ignoreCase = true)
        }.sortedByDescending { it.priority }
    }

    private fun filterByPriority(tasks: ArrayList<Task>): List<Task> {
        if (!hasPriorityFilters()) return tasks

        if (filters.value?.get(0) == false) {
            tasks.removeAll(tasks.filter { it.priority == 1 }.toSet())
        }
        if (filters.value?.get(1) == false) {
            tasks.removeAll(tasks.filter { it.priority == 2 }.toSet())
        }
        if (filters.value?.get(2) == false) {
            tasks.removeAll(tasks.filter { it.priority == 3 }.toSet())
        }
        return tasks.sortedByDescending { it.priority }
    }

    private fun filterByStatus(tasks: ArrayList<Task>): List<Task> {
        if (!hasStatusFilters()) return tasks

        if (filters.value?.get(3) == false) {
            tasks.removeAll(tasks.filter { it.status == TaskStatus.TODO }.toSet())
        }
        if (filters.value?.get(4) == false) {
            tasks.removeAll(tasks.filter { it.status == TaskStatus.IN_PROGRESS }.toSet())
        }
        if (filters.value?.get(5) == false) {
            tasks.removeAll(tasks.filter { it.status == TaskStatus.DONE }.toSet())
        }
        return tasks.sortedByDescending { it.priority }
    }

    fun filter(tasks: List<Task>): List<Task> {
        if (hasFilters()) {
            var filterTasks = filterByPriority(ArrayList(tasks))
            filterTasks = filterByStatus(ArrayList(filterTasks))
            return filterTasks
        }
        return tasks
    }

    fun updateFilters(position: Int) {
        if (filters.value?.get(position) == false) {
            filters.value?.set(position, true)
        } else {
            filters.value?.set(position, false)
        }
        filters.apply {
            postValue(value)
        }
    }

    fun cleanFilters() {
        filters.value = filters.value?.map { false }?.toTypedArray()
        taskList.apply {
            postValue(value)
        }
        filters.apply {
            postValue(value)
        }
    }

    fun hasFilters(): Boolean {
        return filters.value?.contains(true) ?: false
    }

    private fun hasPriorityFilters(): Boolean {
        return filters.value?.copyOfRange(0, 3)?.contains(true) ?: false
    }

    private fun hasStatusFilters(): Boolean {
        return filters.value?.copyOfRange(3, 6)?.contains(true) ?: false
    }

    fun getFilters() = filters.value!!

    suspend fun checkTaskIsEditable(groupId: String): Boolean {
        return groupId == "0" || checkUserIsGroupLeader(groupId)
    }

}