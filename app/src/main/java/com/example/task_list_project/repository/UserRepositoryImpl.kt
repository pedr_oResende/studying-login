package com.example.task_list_project.repository

import com.example.task_list_project.database.GroupDao
import com.example.task_list_project.database.TaskDao
import com.example.task_list_project.database.UserDao
import com.example.task_list_project.model.Group
import com.example.task_list_project.model.Task
import com.example.task_list_project.model.User
import com.example.task_list_project.model.relations.TasksByUser
import com.example.task_list_project.viewmodels.GroupsViewModel

class UserRepositoryImpl(
    private val userDao: UserDao,
    private val taskDao: TaskDao,
    private val groupDao: GroupDao
) : UserRepository {

    override suspend fun getOnlineUser(): User? {
        if (hasOnlineUser())
            return userDao.getOnlineUser(true)
        return null
    }

    override suspend fun hasOnlineUser(): Boolean {
        val users = getUsers()
        val user = users.firstOrNull { user -> user.isOnline }
        return user != null
    }

    override suspend fun addUser(user: User) {
        userDao.addUser(user)
    }

    override suspend fun deleteUser(user: User) {
        userDao.removeUser(user)
    }

    override suspend fun updateUser(user: User) {
        deleteUser(user)
        addUser(user)
    }

    override suspend fun getUsers(): List<User> {
        return userDao.getUsers()
    }

    override suspend fun getUser(email: String): User? {
        val users = getUsers()
        val user = users.firstOrNull { user ->
            user.matchEmail(email)
        }
        return user
    }

    override suspend fun getUserById(id: String): User = userDao.getUserById(id)

    override suspend fun getTasks(): List<Task> = taskDao.getTasks()

    override suspend fun addTask(task: Task) = taskDao.addTask(task)

    override suspend fun removeTask(task: Task) = taskDao.removeTask(task)

    override suspend fun updateTask(task: Task) {
        removeTask(task)
        addTask(task)
    }

    override suspend fun getTasksByUser(userId: String): TasksByUser {
        return taskDao.getTasksByUser(userId)
    }

    override suspend fun getTaskById(taskId: String): Task? {
        return taskDao.getTaskById(taskId)
    }

    override suspend fun getGroups(): List<Group> {
        return groupDao.getGroups()
    }

    override suspend fun addGroup(group: Group) {
        groupDao.addGroup(group)
    }

    override suspend fun removeGroup(group: Group) {
        groupDao.removeGroup(group)
    }

    override suspend fun getGroupsByUser(userGroupsID: String): List<Group> {
        return groupDao.getGroups().filter {
            userGroupsID.contains(it.id) || it.id == GroupsViewModel.ID_NOTES
        }
    }

    override suspend fun getGroupById(id: String): Group {
        return groupDao.getGroupById(id)
    }

    override suspend fun getGroupNameById(id: String): String {
        return getGroupById(id).name
    }

    override suspend fun getTasksByGroup(groupId: String): List<Task> {
        return taskDao.getTasksByGroup(groupId)
    }

    override suspend fun updateGroup(group: Group) {
        removeGroup(group)
        addGroup(group)
    }

}