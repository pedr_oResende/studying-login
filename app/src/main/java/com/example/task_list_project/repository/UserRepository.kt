package com.example.task_list_project.repository

import com.example.task_list_project.model.Group
import com.example.task_list_project.model.Task
import com.example.task_list_project.model.User
import com.example.task_list_project.model.relations.TasksByUser

interface UserRepository {

    suspend fun getOnlineUser(): User?

    suspend fun hasOnlineUser(): Boolean

    suspend fun addUser(user: User)

    suspend fun deleteUser(user: User)

    suspend fun updateUser(user: User)

    suspend fun getUsers(): List<User>

    suspend fun getUser(email: String): User?

    suspend fun getUserById(id: String): User

    suspend fun getTasks(): List<Task>

    suspend fun addTask(task: Task)

    suspend fun removeTask(task: Task)

    suspend fun updateTask(task: Task)

    suspend fun getTasksByUser(userId: String): TasksByUser

    suspend fun getTaskById(taskId: String): Task?

    suspend fun getGroups(): List<Group>

    suspend fun addGroup(group: Group)

    suspend fun removeGroup(group: Group)

    suspend fun getGroupsByUser(userGroupsID: String): List<Group>

    suspend fun getGroupById(id: String): Group

    suspend fun getGroupNameById(id: String): String

    suspend fun getTasksByGroup(groupId: String): List<Task>

    suspend fun updateGroup(group: Group)

}