package com.example.task_list_project.utils

import android.content.Context
import com.example.task_list_project.R
import com.example.task_list_project.activities.MainActivity
import com.example.task_list_project.model.Group
import com.example.task_list_project.viewmodels.GroupsViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DefaultGroups {

    companion object {

        fun setupInitialGroups(context: Context) {
            val myGroups = arrayListOf<Group>()
            myGroups.add(
                Group(
                    id = GroupsViewModel.ID_NOTES,
                    leaderId = null,
                    membersId = null,
                    name = context.getString(R.string.groups_my_tasks),
                    description = ""
                )
            )
            val repository by lazy { MainActivity.repository }
            CoroutineScope(Dispatchers.IO).launch {
                myGroups.forEach { repository.addGroup(it) }
            }
        }
    }
}