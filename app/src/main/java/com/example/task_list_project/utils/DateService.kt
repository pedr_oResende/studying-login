package com.example.task_list_project.utils

import com.example.task_list_project.R
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class DateService {
    companion object {
        private val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.US)

        fun getDateAlertColor(taskDeadline: String): Int {
            val deadline = getDateFormat(taskDeadline)
            val currentDate = getCurrentTimeStamp()
            val days = daysBetweenDates(currentDate, deadline)

            return when {
                days > 2 -> {
                    R.color.low_priority
                }
                days == 1 -> {
                    R.color.medium_priority
                }
                else -> {
                    R.color.high_priority
                }
            }
        }

        private fun getCurrentTimeStamp(): String {
            val now = Date()
            return sdf.format(now)
        }

        private fun getDateFormat(date: String): String {
            return date.replace("/", "-", true)
        }

        private fun daysBetweenDates(date1: String, date2: String): Int {
            val start = sdf.parse(date1)!!
            val end = sdf.parse((date2))!!

            return TimeUnit.DAYS.convert(end.time - start.time, TimeUnit.MILLISECONDS).toInt()
        }
    }
}