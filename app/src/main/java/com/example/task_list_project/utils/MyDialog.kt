package com.example.task_list_project.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.task_list_project.R
import com.example.task_list_project.model.Task
import com.example.task_list_project.viewmodels.TasksViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MyDialog {

    companion object {

        fun getDialogLayout(inflater: LayoutInflater, layoutRes: Int): View {
            return inflater.inflate(layoutRes, null)
        }

        fun setSpinner(spinner: Spinner, strings: Int, context: Context) {
            val adapter = ArrayAdapter.createFromResource(
                context,
                strings,
                android.R.layout.simple_spinner_item
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
    }

}