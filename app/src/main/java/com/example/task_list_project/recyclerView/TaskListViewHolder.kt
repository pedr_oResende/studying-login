package com.example.task_list_project.recyclerView

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R

class TaskListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    var taskTitleTextView: TextView = view.findViewById(R.id.taskTitle)
    var taskContentTextView: TextView = view.findViewById(R.id.taskContent)
    var taskStatusTextView: TextView = view.findViewById(R.id.taskStatus)
    var taskPriorityImageView: ImageView = view.findViewById(R.id.taskPriority)
    var taskDeadlineTextView: TextView = view.findViewById(R.id.taskDeadline)
    var taskDeadlineAlertImageView: ImageView = view.findViewById(R.id.alertIcon)

}