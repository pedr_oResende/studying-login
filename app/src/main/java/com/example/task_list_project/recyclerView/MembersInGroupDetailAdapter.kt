package com.example.task_list_project.recyclerView

class MembersInGroupDetailAdapter(
    names: List<String>,
    private val listener: ItemClickListener,
    private val selectedPosition: Int
) : MembersAdapter(names) {

    interface ItemClickListener {
        fun filterOnClick(position: Int)
    }

    override fun onBindViewHolder(holder: MembersViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        if (selectedPosition != -1 && position == selectedPosition) {
            setItemColorsOnClick(holder.container, holder.itemView.context)
        }
        holder.nameTextView.setOnClickListener {
            listener.filterOnClick(position)
        }
    }
}