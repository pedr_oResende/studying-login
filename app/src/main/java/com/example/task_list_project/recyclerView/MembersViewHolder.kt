package com.example.task_list_project.recyclerView

import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R

class MembersViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    var nameTextView: TextView = view.findViewById(R.id.nameTextView)
    var container: ConstraintLayout = view.findViewById(R.id.memberNameContainer)

}