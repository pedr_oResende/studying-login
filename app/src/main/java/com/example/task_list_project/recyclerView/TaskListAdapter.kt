package com.example.task_list_project.recyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R
import com.example.task_list_project.model.Task
import com.example.task_list_project.model.TaskStatus
import com.example.task_list_project.utils.DateService

class TaskListAdapter(
    private var taskList: List<Task>,
    private val listener: ItemClickListener
) :
    RecyclerView.Adapter<TaskListViewHolder>() {

    interface ItemClickListener {
        fun showRemoveDialogTask(view: View, task: Task)
        fun goToTaskDetail(task: Task)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder_task, parent, false)

        return TaskListViewHolder(view)
    }

    override fun onBindViewHolder(holder: TaskListViewHolder, position: Int) {

        val priority = taskList[position].priority

        holder.taskTitleTextView.text = taskList[position].title
        holder.taskContentTextView.text = cutContentSize(taskList[position].content)
        holder.taskDeadlineTextView.text = taskList[position].deadline
        holder.taskPriorityImageView.setImageResource(Task.getPriorityIcon(priority))
        DrawableCompat.setTint(
            holder.taskDeadlineAlertImageView.drawable,
            ContextCompat.getColor(
                holder.itemView.context,
                DateService.getDateAlertColor(taskList[position].deadline)
            )
        )
        DrawableCompat.setTint(
            holder.taskPriorityImageView.drawable,
            ContextCompat.getColor(holder.itemView.context, Task.getPriorityIconColor(priority))
        )
        holder.taskStatusTextView.text = when (taskList[position].status) {
            TaskStatus.TODO -> holder.itemView.context.getString(R.string.task_status_todo)
            TaskStatus.IN_PROGRESS -> holder.itemView.context.getString(R.string.task_status_in_progress)
            TaskStatus.DONE -> holder.itemView.context.getString(R.string.task_status_done)
        }
        holder.itemView.setOnLongClickListener { view ->
            listener.showRemoveDialogTask(view, taskList[position])
            true
        }
        holder.itemView.setOnClickListener {
            listener.goToTaskDetail(taskList[position])
        }

    }

    override fun getItemCount(): Int {
        return taskList.size
    }

    private fun cutContentSize(content: String): String {
        if (content.length <= Task.CONTENT_SIZE_LIMIT_IN_LIST) return content

        return content.substring(0, Task.CONTENT_SIZE_LIMIT_IN_LIST) + "..."
    }
}

