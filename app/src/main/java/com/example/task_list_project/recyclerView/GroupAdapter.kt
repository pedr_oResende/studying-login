package com.example.task_list_project.recyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R
import com.example.task_list_project.model.Group
import com.example.task_list_project.viewmodels.GroupsViewModel.Companion.ID_NOTES

class GroupAdapter(
    private val groups: List<Group>,
    private val listener: HomeFragmentListener
) : RecyclerView.Adapter<GroupViewHolder>() {

    interface HomeFragmentListener {
        fun goToTasks(groupId: String)
        fun showRemoveDialog(view: View, group: Group)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder_groups, parent, false)

        return GroupViewHolder(view)
    }

    override fun onBindViewHolder(holder: GroupViewHolder, position: Int) {
        val id = groups[position].id
        setupImageViewResource(holder.groupImage, id)
        holder.groupName.text = groups[position].name

        if (id != ID_NOTES) {
            holder.itemView.setOnLongClickListener { view ->
                listener.showRemoveDialog(view, groups[position])
                true
            }
        }
        holder.itemView.setOnClickListener {
            listener.goToTasks(groups[position].id)
        }
    }


    override fun getItemCount(): Int = groups.size

    private fun setupImageViewResource(view: ImageView, id: String) {
        when (id) {
            ID_NOTES -> {
                view.setImageDrawable(
                    AppCompatResources.getDrawable(
                        view.context,
                        R.drawable.ic_notes
                    )
                )
            }
            else -> {
                view.setImageDrawable(
                    AppCompatResources.getDrawable(
                        view.context,
                        R.drawable.ic_group
                    )
                )
            }
        }
    }
}