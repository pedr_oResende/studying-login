package com.example.task_list_project.recyclerView

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R

class TaskCommentsAdapter(
    private val comments: List<String>,
    private val listener: TaskDetailFragmentListener,
    private val isStart: Boolean
) : RecyclerView.Adapter<TaskCommentsViewHolder>() {

    interface TaskDetailFragmentListener {
        fun showRemoveDialogTask(view: View, position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskCommentsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder_comment_list, parent, false)

        return TaskCommentsViewHolder(view)
    }

    override fun onBindViewHolder(holder: TaskCommentsViewHolder, position: Int) {
        holder.commentTextView.text = comments[position]

        if (isStart) {
            holder.commentContainer.gravity = Gravity.START
        } else {
            holder.commentContainer.gravity = Gravity.END
        }

        holder.commentTextView.setOnLongClickListener { view ->
            listener.showRemoveDialogTask(view, position)
            true
        }
    }

    override fun getItemCount(): Int = comments.size

}