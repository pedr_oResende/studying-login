package com.example.task_list_project.recyclerView

import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R

class TaskCommentsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    var commentContainer: LinearLayout = view.findViewById(R.id.commentContainer)
    var commentTextView: TextView = view.findViewById(R.id.commentTextView)

}