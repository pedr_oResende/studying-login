package com.example.task_list_project.recyclerView

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R
import com.example.task_list_project.model.Group

class GroupsInProfileAdapter(
    private val groups: List<Group>,
    private val listener: ProfileFragmentListener
) : RecyclerView.Adapter<MembersViewHolder>() {

    interface  ProfileFragmentListener {
        fun goToTasks(groupId: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MembersViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder_group_member, parent, false)

        return MembersViewHolder(view)
    }

    override fun onBindViewHolder(holder: MembersViewHolder, position: Int) {
        holder.nameTextView.text = groups[position].name

        holder.itemView.setOnClickListener {
            listener.goToTasks(groups[position].id)
        }
    }


    override fun getItemCount(): Int = groups.size
}