package com.example.task_list_project.recyclerView

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R

open class MembersAdapter(
    private val names: List<String>
) : RecyclerView.Adapter<MembersViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MembersViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_holder_group_member, parent, false)

        return MembersViewHolder(view)
    }

    override fun onBindViewHolder(holder: MembersViewHolder, position: Int) {
        holder.nameTextView.text = names[position]
        setItemColors(holder.container, holder.itemView.context, position)
    }

    override fun getItemCount(): Int = names.size


    private fun setItemColors(container: ConstraintLayout, context: Context, position: Int) {
        when (position) {
            0 -> {
                container.background = AppCompatResources.getDrawable(
                    context,
                    R.drawable.bg_circle_button_secondary
                )
            }
            else -> {
                container.background = AppCompatResources.getDrawable(
                    context,
                    R.drawable.bg_circle_button
                )
            }
        }
    }

    fun setItemColorsOnClick(container: ConstraintLayout, context: Context) {
        container.background = AppCompatResources.getDrawable(
            context,
            R.drawable.bg_circle_button_variant
        )
    }

}