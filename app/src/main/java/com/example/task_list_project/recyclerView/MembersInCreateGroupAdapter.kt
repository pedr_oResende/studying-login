package com.example.task_list_project.recyclerView

import android.view.View

class MembersInCreateGroupAdapter(
    names: List<String>,
    private val listener: ItemClickListener
) : MembersAdapter(names){

    interface ItemClickListener {
        fun showRemoveDialog(view: View, position: Int)
        fun userIsLeader(): Boolean
    }

    override fun onBindViewHolder(holder: MembersViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        if (position != 0 && listener.userIsLeader()) {
            holder.container.setOnLongClickListener { view ->
                listener.showRemoveDialog(view, position)
                true
            }
        }
    }
}