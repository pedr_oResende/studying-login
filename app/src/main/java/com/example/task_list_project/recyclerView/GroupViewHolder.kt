package com.example.task_list_project.recyclerView

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.task_list_project.R

class GroupViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    var groupImage: ImageView = view.findViewById(R.id.groupImage)
    var groupName: TextView = view.findViewById(R.id.groupName)

}