package com.example.task_list_project.database

import androidx.room.*
import com.example.task_list_project.model.Task
import com.example.task_list_project.model.relations.TasksByUser

@Dao
interface TaskDao {

    @Query("SELECT * FROM task")
    suspend fun getTasks(): List<Task>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addTask(task: Task)

    @Delete
    suspend fun removeTask(task: Task)

    @Transaction
    @Query("SELECT * FROM users WHERE id = :userId")
    suspend fun getTasksByUser(userId: String): TasksByUser

    @Transaction
    @Query("SELECT * FROM task WHERE groupId = :groupId")
    suspend fun getTasksByGroup(groupId: String): List<Task>

    @Transaction
    @Query("SELECT * FROM task WHERE id = :id")
    suspend fun getTaskById(id: String): Task?

}