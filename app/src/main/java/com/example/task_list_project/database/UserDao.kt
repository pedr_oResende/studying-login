package com.example.task_list_project.database

import androidx.room.*
import com.example.task_list_project.model.User

@Dao
interface UserDao {

    @Query("SELECT * FROM users")
    suspend fun getUsers(): List<User>

    @Query("SELECT * FROM users WHERE id = :id")
    suspend fun getUserById(id: String): User

    @Query("SELECT * FROM users WHERE isOnline = :isOnline")
    suspend fun getOnlineUser(isOnline: Boolean): User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUser(user: User)

    @Delete
    suspend fun removeUser(user: User)
}