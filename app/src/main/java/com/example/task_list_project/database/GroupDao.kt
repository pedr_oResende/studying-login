package com.example.task_list_project.database

import androidx.room.*
import com.example.task_list_project.model.Group

@Dao
interface GroupDao {

    @Query("SELECT * FROM groups")
    suspend fun getGroups(): List<Group>

    @Query("SELECT * FROM groups WHERE id = :id")
    suspend fun getGroupById(id: String): Group

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addGroup(group: Group)

    @Delete
    suspend fun removeGroup(group: Group)

}