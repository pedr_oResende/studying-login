package com.example.task_list_project.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.task_list_project.model.Group
import com.example.task_list_project.model.Task
import com.example.task_list_project.model.User
import com.example.task_list_project.utils.DefaultGroups


const val DATABASE_VERSION = 3

@Database(
    entities = [User::class, Task::class, Group::class],
    version = DATABASE_VERSION
)

abstract class UsersDatabase : RoomDatabase() {

    companion object {
        private const val DATABASE_NAME = "TaskList"

        fun buildDatabase(context: Context): UsersDatabase {
            return Room.databaseBuilder(
                context,
                UsersDatabase::class.java,
                DATABASE_NAME
            ).addCallback(object : Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    DefaultGroups.setupInitialGroups(context)
                    super.onCreate(db)
                }
            }).build()
        }
    }

    abstract fun userDao(): UserDao

    abstract fun taskDao(): TaskDao

    abstract fun groupDao(): GroupDao
}